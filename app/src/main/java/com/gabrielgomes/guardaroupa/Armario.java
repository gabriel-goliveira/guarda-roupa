package com.gabrielgomes.guardaroupa;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.gabrielgomes.guardaroupa.crud.RoupasDAO;

import java.util.ArrayList;
import java.util.List;

public class Armario extends AppCompatActivity {

    private FloatingActionButton fb;
    private ListView lista;
    private RoupasDAO banco;
    List<Roupas> rr = new ArrayList<>();
    ArrayAdapter<String> adapter;
    ArrayList<String>tela = new ArrayList<>();
    private Roupas roupaSuja =null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_armario);
        fb = (FloatingActionButton) findViewById(R.id.fb);




        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inte = new Intent(getBaseContext(), Cadastrar.class);
                startActivityForResult(inte,1);
            }
        });




        banco = new RoupasDAO(this);
        rr = banco.listar();

        for (Roupas r : rr){
            tela.add(r.getDesc() + " | " + r.getMarca());
        }
        lista = (ListView) findViewById(R.id.lista);
        adapter = new ArrayAdapter<String>(getBaseContext(), R.layout.white_list2, R.id.textoLista, tela);
        lista.setAdapter(adapter);


        lista.setLongClickable(true);
        lista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Integer codigo = position;
                roupaSuja = rr.get(position);
                banco.Lavar(roupaSuja);
                Toast.makeText(Armario.this, "Roupa está lavando" , Toast.LENGTH_LONG).show();
                rr.remove(roupaSuja);
                tela.remove(roupaSuja.getDesc() + " | " + roupaSuja.getMarca());
                adapter.notifyDataSetChanged();
                return true;
            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK == resultCode){
            Roupas roupas = new Roupas();
            roupas.setDesc(data.getStringExtra("desc"));
            roupas.setMarca(data.getStringExtra("marca"));
            roupas.setDisp(Integer.parseInt(data.getStringExtra("disp")));

            banco.salvar(roupas);
            rr.add(roupas);
            if(roupas.getDisp()==1){
                tela.add(roupas.getDesc() + " | " + roupas.getMarca());
                adapter.notifyDataSetChanged();
            }

        }
    }


}
