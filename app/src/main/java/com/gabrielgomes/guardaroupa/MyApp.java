package com.gabrielgomes.guardaroupa;

import android.app.Application;
import android.content.Context;

public class MyApp extends Application {

    public static Context mContext;

    @Override
    public void onCreate() {
        mContext = getApplicationContext();
        super.onCreate();
    }

    public static Context getmContext(){
        return mContext;
    }
}
