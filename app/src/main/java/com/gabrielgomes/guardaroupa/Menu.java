package com.gabrielgomes.guardaroupa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Menu extends AppCompatActivity {

    private ImageView roupas;
    private  ImageView lavanderia;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        roupas = (ImageView) findViewById(R.id.btRoupas);
        lavanderia = (ImageView) findViewById(R.id.btLavanderia);



        roupas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(Menu.this, Armario.class);
                startActivity(it);
            }
        });

        lavanderia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Menu.this, Lavanderia.class);
                startActivity(i);
            }
        });
    }
}
