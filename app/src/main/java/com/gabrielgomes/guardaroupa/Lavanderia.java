package com.gabrielgomes.guardaroupa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.gabrielgomes.guardaroupa.crud.RoupasDAO;

import java.util.ArrayList;
import java.util.List;

public class Lavanderia extends AppCompatActivity {

    private ListView lista;
    private RoupasDAO banco;
    List<Roupas> rr = new ArrayList<>();
    ArrayAdapter<String> adapter;
    ArrayList<String>tela = new ArrayList<>();
    private Roupas roupaLimpa =null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lavanderia);


        banco = new RoupasDAO(this);
        rr = banco.listarSujas();

        for (Roupas r : rr){
            tela.add(r.getDesc() + " | " + r.getMarca());
        }
        lista = (ListView) findViewById(R.id.lista);
        adapter = new ArrayAdapter<String>(getBaseContext(), R.layout.white_list2, R.id.textoLista, tela);
        lista.setAdapter(adapter);

        lista.setLongClickable(true);
        lista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Integer codigo = position;
                roupaLimpa = rr.get(position);
                banco.Pendurar(roupaLimpa);
                Toast.makeText(Lavanderia.this, "Roupa está limpa" , Toast.LENGTH_LONG).show();
                rr.remove(roupaLimpa);
                tela.remove(roupaLimpa.getDesc() + " | " + roupaLimpa.getMarca());
                adapter.notifyDataSetChanged();
                return true;
            }
        });
    }

}
