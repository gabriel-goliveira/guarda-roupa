package com.gabrielgomes.guardaroupa;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class Cadastrar extends AppCompatActivity {
    private EditText desc;
    private EditText tipo;
    private EditText cor;
    private EditText marca;
    private CheckBox cb;
    private Button btSalvar;
    int verificador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);

        desc = (EditText) findViewById(R.id.txtDesc);
        marca = (EditText) findViewById(R.id.txtMarca);
        cb = (CheckBox) findViewById(R.id.cb);
        btSalvar = (Button) findViewById(R.id.btSalvar);



      btSalvar.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent it = new Intent();
              it.putExtra("desc", desc.getText().toString());
              it.putExtra("marca", marca.getText().toString());
              if (cb.isChecked()){
                  it.putExtra("disp", "1");
              }else {
                  it.putExtra("disp", "0");
              }

              setResult(Activity.RESULT_OK, it);
              finish();

          }
      });
    }
}
