package com.gabrielgomes.guardaroupa.crud;

//https://developer.android.com/reference/android/database/sqlite/SQLiteOpenHelper#getReadableDatabase%28%29

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gabrielgomes.guardaroupa.MyApp;
import com.gabrielgomes.guardaroupa.Roupas;

import java.util.ArrayList;
import java.util.List;

public class RoupasDAO extends SQLiteOpenHelper {

    public RoupasDAO(Context context){
        super(context,"Roupas",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS Roupas(id INTEGER PRIMARY KEY "+
                "AUTOINCREMENT, descricao TEXT, marca TEXT, disp INTEGER);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Roupas salvar(Roupas roupas){
        SQLiteDatabase db = getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("descricao", roupas.getDesc());
            values.put("marca", roupas.getMarca());
            values.put("disp", roupas.getDisp());

            if(roupas.getID()== null){
                long id = db.insert("Roupas",null, values);
                roupas.setID(id);
            }
        }finally {
            db.close();
        }
        return roupas;
    }

    public void Lavar(Roupas roupas){
        SQLiteDatabase db = getWritableDatabase();
        if (roupas.getID()!=null){
            try {
                roupas.setDisp(0);
                ContentValues values = new ContentValues();
                values.put("disp", roupas.getDisp());
                String[] where = new String[]{String.valueOf(roupas.getID())};
                db.update("Roupas", values, "id = ?", where);
            }finally {
                db.close();
            }
        }

    }

    public void Pendurar(Roupas roupas){
        SQLiteDatabase db = getWritableDatabase();
        if (roupas.getID()!=null){
            try {
                roupas.setDisp(1);
                ContentValues values = new ContentValues();
                values.put("disp", roupas.getDisp());
                String[] where = new String[]{String.valueOf(roupas.getID())};
                db.update("Roupas", values, "id = ?", where);
            }finally {
                db.close();
            }
        }

    }
    public List<Roupas> listar(){
        List<Roupas> limpas = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT * "+
            "FROM Roupas WHERE disp = 1 ", null);

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                Roupas roupas =  new Roupas();
                roupas.setID(cursor.getLong(0));
                roupas.setDesc(cursor.getString(1));
                roupas.setMarca(cursor.getString(2));
                roupas.setDisp(cursor.getInt(3));
                limpas.add(roupas);
                cursor.moveToNext();

            }

            cursor.close();

        }catch (Exception e){
            e.printStackTrace();

        }finally {
            db.close();
        }
        return limpas;
    }



    public List<Roupas> listarSujas(){
        List<Roupas> limpas = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT * "+
                    "FROM Roupas WHERE disp = 0 ", null);

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                Roupas roupas =  new Roupas();
                roupas.setID(cursor.getLong(0));
                roupas.setDesc(cursor.getString(1));
                roupas.setMarca(cursor.getString(2));
                roupas.setDisp(cursor.getInt(3));
                limpas.add(roupas);
                cursor.moveToNext();

            }

            cursor.close();

        }catch (Exception e){
            e.printStackTrace();

        }finally {
            db.close();
        }
        return limpas;
    }


}
